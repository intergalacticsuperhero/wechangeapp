package com.kolloware.wechange.models;

public class ExternalAppInformation {
    public final String appPackage;
    public final String alternativeAppPackage;
    public final Integer appTitleResourceKey;
    public final Integer instructionsResourceKey;
    public final String browserURL;

    public ExternalAppInformation(String inAppPackage,
                                  String inAlternativeAppPackage,
                                  Integer inAppTitleResourceKey,
                                  Integer inInstructionsResourceKey,
                                  String inBrowserURL) {
        this.appPackage = inAppPackage;
        this.alternativeAppPackage = inAlternativeAppPackage;
        this.appTitleResourceKey = inAppTitleResourceKey;
        this.instructionsResourceKey = inInstructionsResourceKey;
        this.browserURL = inBrowserURL;
    }

    public boolean canRunInBrowser() {
        return (browserURL != null);
    }

    public ExternalAppInformation getAlternativeAppInformation() {
        return new ExternalAppInformation(alternativeAppPackage,
                null,
                appTitleResourceKey,
                instructionsResourceKey,
                browserURL);
    }
}
